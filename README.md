# Imdex, the image color indexer

## About this program

Imdex uses [`Pillow`](https://pillow.readthedocs.io/en/stable/) to convert a 256-color image to an array of indexes. Each index represents a color in the 256-color VGA palette.

The output of the procedure is an array of unsigned characters (uint8_t) in a .c file (the stdint.h header is also included).

The name of the output file is the name of the original file, so if the input is called `test.jpg`, the output is `test.c`.

You can see the color palette below:

<img src="screenshots/palette.png" width="250px" />

## Requirements

To convert your pictures to 256-color images which can be used inside RexOS, you will have to install gimp on your computer and import the appropriate 256-color VGA color palette.

To do this, enter the following command and hit enter:

Arch users:

````bash
sudo pacman -S gimp
````

Debian users:

````bash
sudo apt install gimp
````

In this repository you can find a Gimp palette file which I found [here](https://github.com/icebreaker/floppybird/blob/master/data/rgb/palette/VGA.gpl).

You need to import the `VGA.gpl` color palette into Gimp:

- `Windows > Dockable Dialogs > Palettes > right click > New Palette > select the file`
- `File > Open > select your picture`
- `Image > Mode > Indexed > Use custom palette > VGA`
- Be sure to uncheck the "Remove unused and duplicate colors from colormap" option
- Export your picture

Now that you have exported your picture, you will have to install [`Pillow`](https://pillow.readthedocs.io/en/stable/) which is a fork of [`PIL`](https://en.wikipedia.org/wiki/Python_Imaging_Library). This library helps in getting the RGB values for every single pixel in the picture.

To install Pillow, execute the following command:

````bash
pip3 install Pillow
````

## Usage

Simply put the path / name of the input file after the command, like this:

````python
python3 imdex.py <path/to/the/picture>
````

You will see an output file with the same name but with a different extension (.c) like the input file.

Now you can move the output file to the source directory of RexOS and draw it using the following function call:

````C
#include <stdint.h>

extern uint8_t my_image[width * height];
// yes, you also have to set the real size

...

vga_draw_image(
    my_image, // name of the array
    0,        // x offset
    0,        // y offset
    320,      // width
    200       // height
);
````

## Screenshots

Original picture

<img src="input.jpg" width="320px" />

Output file (.c)

<img src="screenshots/output.png" width="320px" />

Result as seen in RexOS using QEMU

<img src="screenshots/result.png" width="320px" />
