import sys, os
from PIL import Image
from palette import palette

def closest_index(color):
	diff = 0
	index = 0
	counter = -1

	for item in palette:
		counter += 1
		curr_diff = abs(item[0] - color[0]) + abs(item[1] - color[1]) + abs(item[2] - color[2])

		if counter == 0 or curr_diff < diff:
			diff = curr_diff
			index = counter

	return index

file_in = sys.argv[1]
file_name = file_in.split('.')[0]
file_out = file_name + '.c'

image = Image.open(file_in)
image_rgb = image.convert('RGB')
width, height = image.size

if os.path.exists(file_out):
	os.remove(file_out)

file = open(file_out, 'x')
file.close() 
file = open(file_out, 'w+')

file.write('#include <stdint.h>\n\nuint8_t ' + file_name + '[] =\n{\n')

for y in range(0, height):
	for x in range(0, width):
		color = image_rgb.getpixel((x,y))

		if x == 0:
			file.write('	')

		file.write(str(closest_index(color)))
		file.write(',')

		if x == width - 1:
			file.write('\n')

file.write('};\n')

file.close() 
